<?php

namespace Drupal\start_end_date_format\Plugin\Field\FieldFormatter;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\datetime_range\Plugin\Field\FieldFormatter\DateRangeDefaultFormatter;

/**
 * Plugin implementation of the 'Start or End Date' formatter.
 *
 * @FieldFormatter(
 *   id = "start_end_date_format",
 *   label = @Translation("Start or End Date"),
 *   field_types = {
 *     "daterange"
 *   }
 * )
 */
class StartEndDateFormatter extends DateRangeDefaultFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'start_end' => 'value',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  protected function getOptions($get_selected = false) {
    $options = [
      'value' => $this->t('Start Date'),
      'end_value' => $this->t('End Date')
    ];
    if ($get_selected) {
      return $options[$this->getSetting('start_end')];
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  protected function formatDate($date) {
    $format_type = $this->getSetting('format_type');
    $timezone = $this->getSetting('timezone_override') ?: $date->getTimezone()->getName();
    return $this->dateFormatter->format($date->getTimestamp(), $format_type, '', $timezone != '' ? $timezone : NULL);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);
    unset($elements['separator']);

    $elements['start_end'] = [
      '#type' => 'select',
      '#title' => $this->t('Start or End Date'),
      '#options' => $this->getOptions(),
      '#default_value' => $this->getSetting('start_end'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $summary[] = $this->t('Start or End Date: @start_end', ['@start_end' => $this->getOptions(true)]);

    $unset_key = null;
    foreach ($summary as $key => $value) {
      if (str_contains($value->__toString(), 'Separator')) {
        $unset_key = $key;
      }
    }
    unset($summary[$unset_key]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      $date = new DrupalDateTime();
      $date = $date->createFromFormat(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, $item->getValue()[$this->getSetting('start_end')]);
      $element[$delta] = [
        '#markup' => $this->formatDate($date),
      ];
    }

    return $element;
  }

}

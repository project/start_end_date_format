Sometimes there are cases where we want to show either Start Date or End Date from a Datetime Range field. This module provides a new format for Datetime Range field to acheive just that!

1. Install the module just like anyother Drupal module.
2. Add a Datetime Range field on your Entity.
3. Goto manage display of that entity and select this 'Start or End Date' format and choose which date value you want to show. (Start or End date)
4. It works with Views too!
